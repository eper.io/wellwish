#!/bin/bash

docker build -t eper.io/wellwish .
docker run -d --restart=always -p 8500:7777 --name wellwish eper.io/wellwish
